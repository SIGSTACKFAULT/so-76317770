// https://on.cypress.io/api

import { FOO } from "../../src/foo";

describe('My First Test', () => {
  it('visits the app root url', () => {
    cy.visit('/')
    cy.contains('h1', 'You did it!')
  })
})
