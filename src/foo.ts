export const FOO = import.meta.url;
// in my real code it uses import.meta.url to contrive the path to a file in the 'public' directory
// *without* import.meta.url, it breaks in cypress component tests